<?php


namespace App\Services;


use PHPUnit\Runner\Exception;

class SampleService
{
    public function divide(int $a, int $b){
        if($b != 0)
            return $a/$b;
        return null;
    }
}