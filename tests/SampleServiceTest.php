<?php

namespace App\Tests;

use App\Services\SampleService;
use PHPUnit\Framework\TestCase;


class SampleServiceTest extends TestCase
{
    public function testDivideWorkTrue(){
        $sampleService = new SampleService();
        $result = $sampleService->divide(4,2);
        $this->assertEquals($result, 2);
    }

    public function testDivideTrue(){
        $sampleService = new SampleService();
        $result = $sampleService->divide(4,2);
        $this->assertEquals($result, 2);
    }

    public function testDivideByZero(){
        $sampleService = new SampleService();
        $result = $sampleService->divide(4,0);
        $this->assertNull($result);

    }

}
