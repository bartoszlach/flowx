<?php

namespace App\Tests;

use App\Entity\UserProfile;
use App\Repository\UserProfileRepository;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserProfileRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    private $newProfile1;
    private $newProfile2;
    private $newProfile3;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $em= $this->entityManager;
        $newProfile1 = new UserProfile();
        $newProfile1->setGivenName("Testing Name 1");
        $date = new DateTime('1999-03-01');

        $newProfile1->setBirthDate($date);
        $this->newProfile1 = $newProfile1;
        $em->persist($newProfile1);
        ////////////////
        $newProfile2 = new UserProfile();
        $newProfile2->setGivenName("Testing Name 2");
        $date = new DateTime('2000-08-22');

        $newProfile2->setBirthDate($date);
        $this->newProfile2 = $newProfile2;
        $em->persist($newProfile2);
        /////////////
        $newProfile3 = new UserProfile();
        $newProfile3->setGivenName("Testing Name 3");
        $date = new DateTime('2001-06-01');

        $newProfile3->setBirthDate($date);
        $this->newProfile3 = $newProfile3;
        $em->persist($newProfile3);

        $em->flush();
    }

    public function testSameAgeAcceptedRange()
    {
        $profiles = $this->entityManager->getRepository(UserProfile::class)->dateFilterByAge(19,19);
        $this->assertCount(1, $profiles);
        $this->assertSame($profiles[0]->getGivenName(),$this->newProfile2->getGivenName());

    }
    public function testAgeLowerAcceptedRange()
    {
        $profiles = $this->entityManager->getRepository(UserProfile::class)->dateFilterByAge(18,19);
//        echo $profiles[0]->getGivenName();
//        echo $profiles[1]->getGivenName();
        $this->assertCount(2, $profiles);

    }
    public function testUpperAgeAcceptedRange()
    {
        $profiles = $this->entityManager->getRepository(UserProfile::class)->dateFilterByAge(19,20);
        $this->assertCount(2, $profiles);
    }

    protected function tearDown()
    {
        $this->entityManager->remove($this->newProfile1);
        $this->entityManager->remove($this->newProfile2);
        $this->entityManager->remove($this->newProfile3);

        $this->entityManager->flush();
    }

//    public function testDataFiltering1()
//    {
//        $repo = new UserProfileRepository;
//        $profilesFromSql = $repo->dateFilter(9,10);
//        $this->assertCount(1, $profilesFromSql, 'Count not expected');
//
//
//    }
}
